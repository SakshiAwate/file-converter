$( document ).ready(function() {
    $(".welcome-messege").show()
    $(".file-selector-pdf-to-word").hide()
    $(".file-selector-word-to-pdf").hide()
    $(".file-selector-merge-pdf").hide()
    $(".file-selector-merge-docx").hide()
    $(".file-selector-image-to-pdf").hide()
});

function myFunction(){
    var e = document.getElementById("selectmenu");
    var opt = e.value;
    if(opt == 4){
        $(".welcome-messege").show()
        $(".file-selector-pdf-to-word").hide()
        $(".file-selector-word-to-pdf").hide()
        $(".file-selector-merge-pdf").hide()
        $(".file-selector-merge-docx").hide()
        $(".file-selector-image-to-pdf").hide()
    }
    else if(opt == 1){
        $(".welcome-messege").hide()
        $(".file-selector-pdf-to-word").show()
        $(".file-selector-word-to-pdf").hide()
        $(".file-selector-merge-pdf").hide()
        $(".file-selector-merge-docx").hide()
        $(".file-selector-image-to-pdf").hide()
    }
    else if(opt == 2){
        $(".welcome-messege").hide()
        $(".file-selector-pdf-to-word").hide()
        $(".file-selector-word-to-pdf").show()
        $(".file-selector-merge-pdf").hide()
        $(".file-selector-merge-docx").hide()
        $(".file-selector-image-to-pdf").hide()
    }
    else if(opt == 3){
        $(".welcome-messege").hide()
        $(".file-selector-pdf-to-word").hide()
        $(".file-selector-word-to-pdf").hide()
        $(".file-selector-merge-pdf").show()
        $(".file-selector-merge-docx").hide()
        $(".file-selector-image-to-pdf").hide()
    }
    else if(opt == 5){
        $(".welcome-messege").hide()
        $(".file-selector-pdf-to-word").hide()
        $(".file-selector-word-to-pdf").hide()
        $(".file-selector-merge-pdf").hide()
        $(".file-selector-merge-docx").show()
        $(".file-selector-image-to-pdf").hide()
    }
    else if(opt == 6){
        $(".welcome-messege").hide()
        $(".file-selector-pdf-to-word").hide()
        $(".file-selector-word-to-pdf").hide()
        $(".file-selector-merge-pdf").hide()
        $(".file-selector-merge-docx").hide()
        $(".file-selector-image-to-pdf").show()
    }
}


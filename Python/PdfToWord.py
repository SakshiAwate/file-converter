import json
import os

# json
fileChecker = open('data.json','r')
loader = json.load(fileChecker)
 
operation = loader['operation']

if(operation == 1):
    #PDF To Word
    import win32com.client
    
    file_name = loader['name'] 

    word = win32com.client.Dispatch("Word.Application")
    word.visible = 0

    doc_pdf ="C:/xampp/htdocs/Workspace/DemoFiles/"+ file_name
    input_file = os.path.abspath(doc_pdf)
    wb = word.Documents.Open(input_file)
    output_file = os.path.abspath(doc_pdf[0:-4]+ "docx".format())
    wb.SaveAs2(output_file,FileFormat=16)
    print("Conversion Completed...")
    wb.Close()
    word.Quit()
    os.startfile(output_file+".docx")

elif(operation == 2):
    # WORD TO PDF
    import win32com.client

    file_name = loader['name'] 

    word = win32com.client.Dispatch("Word.Application")
    word.visible = 0

    doc_pdf = "C:/xampp/htdocs/Workspace/DemoFiles/"+ file_name
    input_file = os.path.abspath(doc_pdf)
    wb = word.Documents.Open(input_file)
    output_file = os.path.abspath(doc_pdf[0:-4]+ "pdf".format())
    wb.SaveAs2(output_file,FileFormat=17)
    print("Conversion Completed...")
    wb.Close()
    word.Quit()
    print(output_file)
    os.startfile(output_file)

elif(operation == 3):
    # Multiple Merging PDF

    file_names = loader['0'] 
    files = [None] * len(file_names)

    for i in range(0, len(file_names)):
        files[i] = "C:/xampp/htdocs/Workspace/DemoFiles/"+file_names[i] 
    #print(files)
    
    from PyPDF2 import PdfFileMerger, PdfFileReader
    

    mergedObject = PdfFileMerger()

    for x in range(len(files)):
        mergedObject.append(PdfFileReader(files[x], 'rb'))

    mergedObject.write("C:/xampp/htdocs/Workspace/DemoFiles/FileVertertMergedpdf.pdf")
    print("Merging Completed...")
    os.startfile('C:/xampp/htdocs/Workspace/DemoFiles/FileVertertMergedpdf.pdf')


elif(operation == 4):

    # Merger Word Documents

    file_names = loader['0'] 
    files = [None] * len(file_names)

    for i in range(0, len(file_names)):
        files[i] = "C:/xampp/htdocs/Workspace/DemoFiles/"+file_names[i] 
    #print(files)

    from docx import Document

    merged_document = Document()

    for index, file in enumerate(files):
        sub_doc = Document(file)

        if index < len(files)-1:
            sub_doc.add_page_break()

        for element in sub_doc.element.body:
            merged_document.element.body.append(element)

    merged_document.save('C:/xampp/htdocs/Workspace/DemoFiles/FileverterDocx.docx')
    os.startfile('C:/xampp/htdocs/Workspace/DemoFiles/FileVerterDocx.docx')

elif(operation == 5):
    from PIL import Image
    from fpdf import FPDF
    pdf = FPDF()
    
    file_names = loader['0'] 
    imagelist = [None] * len(file_names)
    for i in range(0, len(file_names)):
        imagelist[i] = "C:/xampp/htdocs/Workspace/DemoFiles/"+file_names[i] 
    #print(files)

    from fpdf import FPDF
    pdf = FPDF()

    # imagelist is the list with all image filenames
    for image in imagelist:
        pdf.add_page()
        pdf.image(image,0,0,210,300)
    pdf.output("C:/xampp/htdocs/Workspace/DemoFiles/FilVerterImageToPDF.pdf", "F")
    os.startfile('C:/xampp/htdocs/Workspace/DemoFiles/FilVerterImageToPDF.pdf')
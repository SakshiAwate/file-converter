$( document ).ready(function() {
    var check;
    $("#btnpdftoword").click(function(){
        check = $("#filepdf").val()
        if(check == ""){
            $('#errdiv').html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>OOPS!!!</strong> Please select a PDF file first.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
        }
        else{
            $("#formpdftoword").submit();
        }
    });

    $("#btnwordtopdf").click(function(){
        check = $("#fileword").val()
        if(check == ""){
            $('#errdiv').html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>OOPS!!!</strong> Please select a Word(.docx) file first.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
        }
        else{
            $("#formwordtopdf").submit();
        }
    });

    $("#btnmergepdf").click(function(){
        check = $("#filemergepdf").val()
        if(check == ""){
            $('#errdiv').html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>OOPS!!!</strong> Please select Multiple PDF file\'s first.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
        }
        else{
            $("#formmergepdf").submit();
        }
    });

    $("#btnmergedocx").click(function(){
        check = $("#filemergedocx").val()
        if(check == ""){
            $('#errdiv').html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>OOPS!!!</strong> Please select Multiple Word(.docx) file\'s first.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
        }
        else{
            $("#formmergedocx").submit();
        }
    });

    $("#btnimagetopdf").click(function(){
        check = $("#fileimagetopdf").val()
        if(check == ""){
            $("#errdiv").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>OOPS!!!</strong> Please select Image file\'s first.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
        }
        else{
            $("#formimagetopdf").submit();
        }
    });
});